factorial(0,1).
factorial(1,1).
factorial(X,Y) :- X>1, X1 is X-1, factorial(X1,Y1), Y is Y1*X.


fibonacci(1,0).
fibonacci(2,1).
fibonacci(3,1).
fibonacci(X,Y) :- X>3, X1 is X-1, fibonacci(X1,Y1), X2 is X-2, fibonacci(X2,Y2), Y is Y1+Y2.


potencia(_X,0,1).
potencia(X,1,X).
potencia(X,Y,Z) :- Y<0 -> potencia((1/X),abs(Y),Z);(Y1 is Y-1, potencia(X,Y1,Z1), Z is X*Z1).


mcd(X,X,X). 
mcd(X,Y,M):- X < Y, Y1 is Y - X, mcd(X,Y1,M). 
mcd(X,Y,M):- X > Y, mcd(Y,X,M).


horoscopo(21,1,20,2,"ACUARIO").
horoscopo(21,2,20,3,"PISCIS").
horoscopo(21,3,20,4,"ARIES").
horoscopo(21,4,20,5,"TAURO").
horoscopo(21,5,20,6,"GEMINIS").
horoscopo(21,6,20,7,"CANCER").
horoscopo(21,7,20,8,"LEO").
horoscopo(21,8,20,9,"VIRGO").
horoscopo(21,9,20,10,"LIBRA").
horoscopo(21,10,20,11,"ESCORPION").
horoscopo(21,11,20,12,"SAGITARIO").
horoscopo(21,12,20,1,"CAPRICORNIO").
clasificarHoroscopo(DIA,MES,SIGNO) :- DIA=<20 -> (M is MES-1, horoscopo(21,M,20,MES,SIGNO));(M is MES+1, horoscopo(21,MES,20,M,SIGNO)).