class Triqui {
  constructor() {
    this.posiciones = [
      ['', '', ''],
      ['', '', ''],
      ['', '', '']
    ];
    this.ganadorJuego = false
    this.turnoJuego = 'X'
    this.turnoRival = '0'
    this.contador = 9
    this.lugares = [
      [2, 1, 2],
      [1, 3, 1],
      [2, 1, 2]
    ]
    this.linea = ''
  }

  seleccionarPosicion = (nivel) => {
    this.actualizarPosiciones()
    let i, j
    if (nivel == 1) {
      let libre = true
      i = Math.floor(Math.random() * 3)
      j = Math.floor(Math.random() * 3)
      while (this.lugares[i][j] == 0) {
        i = Math.floor(Math.random() * 3)
        j = Math.floor(Math.random() * 3)
      }
      this.lugares[i][j] = 0
    } else if (nivel == 2) {
      let libre = true
      let max = this.maximo()
      i = Math.floor(Math.random() * 3)
      j = Math.floor(Math.random() * 3)
      while (this.lugares[i][j] != max) {
        i = Math.floor(Math.random() * 3)
        j = Math.floor(Math.random() * 3)
      }
      this.lugares[i][j] = 0
    }
    // console.log(this.lugares[i][j]);

    this.contador--
    return { i, j }
  }
  maximo = () => {
    let maximo = 0
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (this.lugares[i][j] > maximo) {
          maximo = this.lugares[i][j]
        }
      }
    }
    return maximo
  }

  actualizarPosiciones = () => {
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (this.posiciones[i][j] !== '') {
          this.lugares[i][j] = 0
        }
      }
    }
  }

  cambiarTurno = () => {
    if (this.turnoJuego === 'X') {
      this.turnoJuego = 'O'
      this.turnoRival = 'X'
    } else {
      this.turnoJuego = 'X'
      this.turnoRival = 'O'
    }
  }
  validarGanador = (turno) => {
    let ganador = false
    for (let i = 0; i < 3; i++) {
      let contFila = 0
      let contColumna = 0
      for (let j = 0; j < 3; j++) {
        if (this.posiciones[i][j] === turno)
          contFila++
        if (this.posiciones[j][i] === turno)
          contColumna++
        if (contColumna === 3) {
          ganador = true
          this.linea = 'columna' + (i + 1);
          break
        }
        if (contFila === 3) {
          this.linea = 'fila' + (j + 1);
          ganador = true
          break
        }
      }
    }
    let contDiagonal1 = 0
    let contDiagonal2 = 0
    for (let i = 0; i < 3; i++) {
      if (this.posiciones[i][i] === turno)
        contDiagonal1 += 1
      if (this.posiciones[i][2 - i] === turno)
        contDiagonal2 += 1
      if (contDiagonal1 === 3) {
        this.linea = 'diagonal1'
        ganador = true
        break
      }
      if (contDiagonal2 === 3) {
        this.linea = 'diagonal2'
        ganador = true
        break
      }
    }
    return ganador
  }

  intentarGanar = () => {
    let flag = true
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (this.posiciones[i][j] === '' && flag) {
          this.posiciones[i][j] = this.turnoJuego
          if (this.validarGanador(this.turnoJuego)) {
            this.ganadorJuego = true
            flag = false
          } else {
            this.posiciones[i][j] = ''
          }
        }
      }
    }
  }

  bloquearRival = () => {
    let flag = true
    for (let i = 0; i < 3; i++) {
      for (let j = 0; j < 3; j++) {
        if (this.posiciones[i][j] === '' && flag) {
          this.posiciones[i][j] = this.turnoRival
          if (this.validarGanador(this.turnoRival)) {
            this.posiciones[i][j] = this.turnoJuego
            contador--
            flag = false
          } else {
            this.posiciones[i][j] = ''
          }
        }
      }
    }
  }

  cualquierLugar = () => {
    let { i, j } = this.seleccionarPosicion(2)
    this.posiciones[i][j] = this.turnoJuego
  }

  jugarTurno = () => {
    if (!this.ganadorJuego) {
      let tableroAntes = copiar(this.posiciones)
      this.intentarGanar()
      if (esIgual(tableroAntes, this.posiciones) && !this.ganadorJuego) {
        this.bloquearRival()
      }
      if (esIgual(tableroAntes, this.posiciones) && !this.ganadorJuego) {
        // console.log('entró a cualquiera');
        if ((this.posiciones[0][0] === this.turnoRival && this.posiciones[2][2] === this.turnoRival) || (this.posiciones[0][2] === this.turnoRival && this.posiciones[2][0] === this.turnoRival)) {
          this.posiciones[0][1] = this.turnoJuego
          contador--
        } else {
          this.cualquierLugar()
        }
      }
      console.log('Turno', this.turnoJuego);
      imprimir(this.posiciones);
      dibujarTriqui(this.posiciones)
      dibujarPasoAPaso(this.posiciones, this.turnoJuego)
      if (!this.ganadorJuego) {
        this.cambiarTurno()
        this.linea = ''
      }
    }
  }

  jugarTurnoHumano = (i, j) => {
    this.posiciones[i][j] = this.turnoJuego
    imprimir(this.posiciones);
    dibujarTriqui(this.posiciones)
    dibujarPasoAPaso(this.posiciones, this.turnoJuego)
    if (this.validarGanador(this.turnoJuego)) {
      this.ganadorJuego = true
    }
    this.cambiarTurno()
    this.contador--
  }
}

// 
// 
// Fin clase con la logica, inicio parte visual y manejo del control del usuario
// 
// 
let triqui = null;
const btnHumano = document.getElementById('boton-h')
const btnMaquina = document.getElementById('boton-m')
const tablero = document.getElementById('tablero')
const fichas = document.querySelectorAll('.fila')
const pasos = document.getElementById('pasos')
const barra = document.getElementById('barra')
const matriz = [[], [], []]
const matrizAux = [[], [], []]
let contador = 0

for (let i = 0; i < 3; i++) {
  matriz[i] = fichas[i].querySelectorAll('.casilla p')
  matrizAux[i] = fichas[i].querySelectorAll('.casilla')
}

btnMaquina.addEventListener('click', crearJuegoMaquina)
btnHumano.addEventListener('click', crearJuegoHumano)

function crearJuegoMaquina() {
  btnMaquina.classList.add('selected')
  btnHumano.classList.remove('selected')
  resetContent()
  triqui = new Triqui()

  for (let i = 1; i < 10; i++) {
    setTimeout(() => {
      jugarTurno(triqui)
    }, 250 * i)
  }
}

function crearJuegoHumano() {
  btnMaquina.classList.remove('selected')
  btnHumano.classList.add('selected')
  resetContent()
  quitarListener()
  triqui = new Triqui()
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      matrizAux[i][j].addEventListener('click', handleCrearJuego)
      matrizAux[i][j].classList.add('cursor')
    }
  }
}
function handleCrearJuego(e) {

  let pos = e.target.id.split(':')
  let i = pos[0]
  let j = pos[1]
  jugarTurnoH(triqui, i, j)
  jugarTurno(triqui)
  console.log(pos);
}
function quitarListener() {
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      matrizAux[i][j].removeEventListener('click', handleCrearJuego)
      matrizAux[i][j].classList.remove('cursor')
    }
  }
}

function jugarTurno(triqui) {
  console.log(triqui.ganadorJuego);
  if (!triqui.ganadorJuego && triqui.contador > 0) {
    triqui.jugarTurno()
    if (triqui.ganadorJuego) {
      switchLinea(triqui)
    }
    if (triqui.contador === 1) {
      btnMaquina.classList.remove('selected')
      btnHumano.classList.remove('selected')
    }
  } else {
    switchLinea(triqui)
    btnMaquina.classList.remove('selected')
    btnHumano.classList.remove('selected')
  }
}

function jugarTurnoH(triqui, i, j) {
  console.log(triqui.ganadorJuego);
  if (!triqui.ganadorJuego && triqui.contador > 0) {
    triqui.jugarTurnoHumano(i, j)
    if (triqui.ganadorJuego) {
      switchLinea(triqui)
      btnMaquina.classList.remove('selected')
      btnHumano.classList.remove('selected')
    }
  } else {
    switchLinea(triqui)
    btnMaquina.classList.remove('selected')
    btnHumano.classList.remove('selected')
  }
}

function switchLinea(triqui) {
  switch (triqui.linea) {
    case 'fila1':
      barra.classList.add('agrandar')
      barra.classList.add('fila1')
      break;
    case 'fila2':
      barra.classList.add('agrandar')
      barra.classList.add('fila2')
      break;
    case 'fila3':
      barra.classList.add('agrandar')
      barra.classList.add('fila3')
      break;
    case 'columna1':
      barra.classList.add('agrandar')
      barra.classList.add('columna1')
      break;
    case 'columna2':
      barra.classList.add('agrandar')
      barra.classList.add('columna2')
      break;
    case 'columna3':
      barra.classList.add('agrandar')
      barra.classList.add('columna3')
      break;
    case 'diagonal1':
      barra.classList.add('agrandar')
      barra.classList.add('diagonal1')
      break;
    case 'diagonal2':
      barra.classList.add('agrandar')
      barra.classList.add('diagonal2')
      break;
    default:
      alert('Empate')
      break;
  }
}

function dibujarTriqui(arreglo) {
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      if (arreglo[i][j] === 'X') {
        matriz[i][j].innerText = '+'
        matriz[i][j].style = 'transform:rotate(45deg);font-size:100px';
      } else {
        matriz[i][j].innerText = arreglo[i][j]
      }
    }
  }
}

function dibujarPasoAPaso(arreglo, turno) {
  let componente = `<div class="paso"><p>Turno ${turno}</p><div class="tablero">\n`
  for (let i = 0; i < 3; i++) {
    componente += '<div class="fila">'
    for (let j = 0; j < 3; j++) {
      componente += `<div class="casilla">${arreglo[i][j]}</div>\n`
    }
    componente += '</div>\n'
  }
  componente += '</div></div>'
  pasos.insertAdjacentHTML("beforeend", componente)
}

function imprimir(arreglo) {
  arreglo.forEach(elemento => {
    let fila = ''
    elemento.forEach(item => {
      fila += '\t' + item
    })
    // console.log(fila);
  });
}

function resetContent() {
  barra.classList.remove('agrandar')
  barra.classList.remove('fila1')
  barra.classList.remove('fila2')
  barra.classList.remove('fila3')
  barra.classList.remove('columna1')
  barra.classList.remove('columna2')
  barra.classList.remove('columna3')
  barra.classList.remove('diagonal1')
  barra.classList.remove('diagonal2')
  for (let i = 0; i < 3; i++) {
    for (let j = 0; j < 3; j++) {
      matriz[i][j].style = '';
      matriz[i][j].innerText = '';
    }
  }
  pasos.innerHTML = ""
}

// 
// 
// Funciones auxiliares
// 
// 
function esIgual(arreglo1, arreglo2) {
  for (let i = 0; i < arreglo1.length; i++) {
    for (let j = 0; j < arreglo1[i].length; j++) {
      if (arreglo1[i][j] !== arreglo2[i][j]) {
        return false
      }
    }
  }
  return true
}
function copiar(array) {
  let copia = []
  for (let i = 0; i < array.length; i++) {
    copia.push([])
    for (let j = 0; j < array[i].length; j++) {
      copia[i].push(array[i][j]);
    }
  }
  return copia
}

